# RasPot
Honeypot designed to run on the Raspian OS and Raspberry Pi's

1) Git clone https://gitlab.com/Omer.Tech/RasPot.git
2) sudo chmod 750 -R RasPot
3) cd RasPot
4) ./Install.sh
5) Follow instructions and enjoy :)

NOTE: You will need to setup a gmail account with multifactor authentication and an application password in order to receive email alerts.
You can use your personal account, or setup an alternative account if you'd like.
https://support.google.com/accounts/answer/185833?hl=en